class Airplane:
    def __init__(self, id, requestTime, timeSlot, timeLength):
        self.id = id
        self.requestTime = requestTime
        self.timeSlot = timeSlot
        self.timeLength = timeLength

    def getID(self):
        print(self.id)

    def getRequestTime(self):
        print(self.requestTime)

    def getTimeSlot(self):
        print(self.timeSlot)

    def getTimeLength(self):
        print(self.timeLength)

    def toString(self):
        print("Airplane ID: " + self.id +
              "Airplane Request Time: " + self.requestTime +
              "Airplane time slot: " + str(self.timeSlot) +
              "Time to take off: " + str(self.timeLength))