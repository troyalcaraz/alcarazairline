This is a README for Airline Python Project

Troy Alcaraz

In this project you will use the Python programming language to simulate aircraft take 
off time slots at an airport.

You need to keep track of the information needed to schedule the airstrip resource: 
request identifier, request submission time, time slot requested, 
length of time requested, actual start time, actual end time. You should do this by 
creating a class to hold all this info.

You need to have a queue of airplanes waiting before they can take off. You should be 
able to print out the status of the queue as time moves along. You may do this by using 
a built-in data structure or by creating a class.

You should be able to load the requests from a file.